from PIL import Image
import PIL
import os
import glob

sourceDir = "Source"
destinationDir = "Results"

pngExtension = ".png"

postFixes = [ "2", "1", "0" ]

#------------------------------------------------------------------
def BuildPath(in_a, in_b):
	return os.path.join(in_a, in_b)

#------------------------------------------------------------------
def main():
	for fileName in os.listdir(sourceDir):
		filePath = os.path.join(sourceDir, fileName)
		if pngExtension in filePath:
			image = Image.open(filePath).convert("RGBA")
			filePath = os.path.splitext(filePath)[0]
			newFileName = fileName + '_' + '3' + pngExtension
			image.save(BuildPath(destinationDir, newFileName))

			for x in range(3):
				newWidth = int(image.size[0] / 2)
				newHeight = int(image.size[1] / 2)
				image = image.resize((newWidth, newHeight), PIL.Image.ANTIALIAS)
				newFileName = fileName + '_' + postFixes[x] + pngExtension
				image.save(BuildPath(destinationDir, newFileName), quality=100, optimize=True)

#------------------------------------------------------------------
if __name__== "__main__":
  main()